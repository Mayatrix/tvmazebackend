﻿using Microsoft.Extensions.DependencyInjection;
using TVMazeDatabase;
public class Startup
{
    public void ConfigureServices(IServiceCollection services)
        => services.AddDbContext<TVMazeDbContext>();
}