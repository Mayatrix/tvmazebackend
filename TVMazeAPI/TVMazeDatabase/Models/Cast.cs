﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVMazeDatabase.Models
{
    public class Cast
    {
        public int Id { get; set; }

        public int TvMazeId { get; set; }

        public Show Show { get; set; }

        public string Name { get; set; }

        public DateTime? Birthday { get; set; }
    }
}
