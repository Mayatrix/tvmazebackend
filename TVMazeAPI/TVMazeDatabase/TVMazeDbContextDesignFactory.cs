﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace TVMazeDatabase
{
    public class TVMazeDbContextDesignFactory : IDesignTimeDbContextFactory<TVMazeDbContext>
    {
        public TVMazeDbContext CreateDbContext(string[] args)
        {
            var builder = new ConfigurationBuilder();
            builder.AddEnvironmentVariables();
            IConfigurationRoot config = builder.Build();

            var optionsBuilder = new DbContextOptionsBuilder<TVMazeDbContext>();
            optionsBuilder.UseSqlServer("Data Source=DESKTOP-6C6LDLA\\SQLEXPRESS;Initial Catalog=TvMazeDB;TrustServerCertificate=True;Trusted_Connection=True;MultipleActiveResultSets=true");

            return new TVMazeDbContext(optionsBuilder.Options);
        }
        
    }
}
