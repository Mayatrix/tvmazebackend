﻿
using Newtonsoft.Json.Converters;

namespace DMARCApi
{
    public class DateFormatConverter : IsoDateTimeConverter
    {
        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}
