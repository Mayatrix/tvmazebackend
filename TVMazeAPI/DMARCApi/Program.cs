using TVMazeDatabase;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Http;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

builder.Services.AddSwaggerGen(c =>
{
    c.IncludeXmlComments(xmlFile);
});
ConfigureServices(builder.Services);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseCors(builder => builder
     .AllowAnyOrigin()
     .AllowAnyMethod()
     .AllowAnyHeader());
}

void ConfigureServices(IServiceCollection services)
{
    services.AddTransient<TvMazeRepository>();
    services.AddDbContext<TVMazeDbContext>(options =>
                options.UseSqlServer("Data Source=DESKTOP-6C6LDLA\\SQLEXPRESS;Initial Catalog=TvMazeDB;TrustServerCertificate=True;Trusted_Connection=True;MultipleActiveResultSets=true")
            );
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
