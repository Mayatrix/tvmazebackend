﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TVMazeApiClient
{
    public class TvMazeApiClient
    {
        private const string BASE_URL = "http://api.tvmaze.com";

        private readonly HttpClient _httpClient;

        public TvMazeApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));

            _httpClient.BaseAddress = new Uri(BASE_URL);
        }

        public async Task<List<ShowHeader>> GetShowsAsync(int page)
        {
            return await GetJsonAsync<List<ShowHeader>>($"/shows?page={page}");
        }

        public async Task<List<CastEntry>> GetCastAsync(int showId)
        {
            return await GetJsonAsync<List<CastEntry>>($"/shows/{showId}/cast");
        }

        private async Task<T> GetJsonAsync<T>(string url)
        {
            HttpResponseMessage response = await _httpClient.GetAsync(url);

            if (!response.IsSuccessStatusCode)
            {
                switch (response.StatusCode)
                {
                    case HttpStatusCode.NotFound:
                        throw new NotFoundException();
                    default:
                        throw new ApiErrorException(response.StatusCode);
                }
            }

            string content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}
