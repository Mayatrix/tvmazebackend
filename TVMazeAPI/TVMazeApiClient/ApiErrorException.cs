﻿using System;
using System.Net;

namespace TVMazeApiClient
{
    public class ApiErrorException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        public ApiErrorException(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }
    }
}
