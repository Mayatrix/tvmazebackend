﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Http;
using TVMazeApiClient;
using TVMazeDatabase;
using TVMazeScraper;

namespace TVMazeScraperRunner
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
            builder.AddEnvironmentVariables();
            IConfigurationRoot configuration = builder.Build();

            var services = new ServiceCollection();
            ConfigureServices(services, configuration);

            ServiceProvider serviceProvider = services.BuildServiceProvider();
            var scraper = serviceProvider.GetRequiredService<TvMazeScraper>();

            await scraper.RunAsync();

            serviceProvider.Dispose();
        }

        private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<TvMazeScraper>();

            services.AddTransient<TvMazeRepository>();

            services.AddTransient<TvMazeApiClient>();

            // TODO: investigate if it blocks the whole HttpClient instance
            // TODO: investigate console app responsiveness when waiting
            services.AddHttpClient<TvMazeApiClient>()
                .AddPolicyHandler(RetryPolicyProvider.Get());

            services.AddDbContext<TVMazeDbContext>(options =>
                options.UseSqlServer("Data Source=DESKTOP-6C6LDLA\\SQLEXPRESS;Initial Catalog=TvMazeDB;TrustServerCertificate=True;Trusted_Connection=True;MultipleActiveResultSets=true")
            );

            services.AddLogging(builder => builder.AddConsole());
        }
    }
}